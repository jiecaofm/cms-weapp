import lazyLoading from './lazyLoading'

export default {
  name: 'Add',
  path: '/add',
  meta: {
    icon: 'fa-bar-chart-o',
    expanded: false
  },
  component: lazyLoading('add', true),
}
