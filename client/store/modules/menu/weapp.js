import lazyLoading from './lazyLoading'

export default {
  meta: {
    label: '微信小程序',
    icon: 'fa-laptop',
    expanded: false
  },

  children: [
    {
      name: '天天向上',
      path: '/dayup',
      component: lazyLoading('weapp/Dayup')
    },
    {
      name: '电影',
      path: '/movie',
      component: lazyLoading('weapp/Movie')
    },
    {
      name: '每天一个段子',
      path: '/episode',
      component: lazyLoading('weapp/Episode')
    },
    {
      name: 'mac小技巧',
      path: '/mac',
      component: lazyLoading('weapp/Mac')
    },
    {
      name: 'Excel小技巧',
      path: '/excel',
      component: lazyLoading('weapp/Excel')
    }
  ]
}
