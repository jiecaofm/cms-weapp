import Vue from 'vue'
import Router from 'vue-router'
import menuModule from 'vuex-store/modules/menu'
Vue.use(Router)

export default new Router({
  mode: 'hash', // Demo is living in GitHub.io, so required!
  linkActiveClass: 'is-active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      name: 'Home',
      path: '/',
      component: require('../views/Home')
    },
    // {
    //   name: 'Login',
    //   path: '/login',
    //   component: require('../layout/Login')
    // },
    {
      name: '天天向上详情',
      path: '/detail',
      component: require('../views/weapp/Detail')
    },
    {
      name: '电影详情',
      path: '/movieDetail',
      component: require('../views/weapp/MovieDetail')
    },
    {
      name: '每天一个段子详情',
      path: '/episodeDetail',
      component: require('../views/weapp/EpisodeDetail')
    },
    {
      name: 'mac小技巧详情',
      path: '/macDetail',
      component: require('../views/weapp/MacDetail')
    },
    {
      name: 'excel小技巧详情',
      path: '/excelDetail',
      component: require('../views/weapp/ExcelDetail')
    },
    ...generateRoutesFromMenu(menuModule.state.items),
    {
      path: '*',
      redirect: '/'
    }
  ]
})

// Menu should have 2 levels.
function generateRoutesFromMenu (menu = [], routes = []) {
  for (let i = 0, l = menu.length; i < l; i++) {
    let item = menu[i]
    if (item.path) {
      routes.push(item)
    }
    if (!item.component) {
      generateRoutesFromMenu(item.children, routes)
    }
  }
  return routes
}
