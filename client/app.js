import Vue from 'vue'
import Resource from 'vue-resource'
import NProgress from 'vue-nprogress'
import { sync } from 'vuex-router-sync'
import App from './App.vue'
import router from './router'
import store from './store'
import * as filters from './filters'
import { TOGGLE_SIDEBAR } from 'vuex-store/mutation-types'

Vue.use(Resource)
Vue.use(NProgress)

// Vue.http.options.root = 'https://wxapp.test.jiecao.fm'
Vue.http.options.root = 'https://wxapp.jiecao.fm'
Vue.http.options.emulateJSON = true

// Enable devtools
Vue.config.devtools = true

sync(store, router)

const nprogress = new NProgress({ parent: '.nprogress-container' })

const { state } = store
// router.beforeEach((route, redirect, next, to) => {
// router.beforeEach((transition) => {
// router.beforeEach(({to, from, next}) => {
//   if (state.app.device.isMobile && state.app.sidebar.opened) {
//     store.commit(TOGGLE_SIDEBAR, false)
//   }
//   next({
//     path: '/excel',
//     query: { redirect: to.fullPath }
//   })
  // console.log('to.path:' + to.path)
  // console.log('from.path:' + from.path)
  // let toPath = to.path
  // let fromPath = from.path
  // console.log(`to: ${toPath} from: ${fromPath}`)
  // if (toPath.replace(/[^/]/g, '').length > 1) {
  //   router.app.isIndex = false
  // } else {
  //   let depath = toPath === '/' || toPath === '/invite' || toPath === '/rank'
  //   router.app.isIndex = depath ? 0 : 1
  // }
  // next()
  // var loginState = sessionStorage.getItem('loginState')
  // if (!loginState && route.path !== '/login') {
  //   return next({ path: '/login' })
  // }
  // next()
  // if (!sessionStorage.getItem('loginState')) {
  //   router.next({
  //     path: '/login',
  //     query: { redirect: transition.fullPath }
  //   })
  // } else {
  // }
// })
// router.beforeEach(function (transition, next) {
//   // if (transition.to.fullPath !== '/') {
//   // if (transition.to.path !== '/') {
//   //   window.removeEventListener('scroll', saveDemoScrollTop, false)
//   // }
//   console.log('transition')
//   console.log(transition)
//   console.log(next)
//   if (sessionStorage.getItem('loginState') !== '1' && transition.path !== '/') {
//     console.log('nologin')
//     next()
//   } else {
//     // transition.next()
//   }
//   // router.replace({path: '/'})
// })

router.beforeEach((route, redirect, next) => {
  console.log('route')
  console.log(route)
  if (sessionStorage.getItem('loginState') !== '1' && route.path !== '/') {
    console.log('/nologin')
    return next({path: '/'})
  } else {
    if (state.app.device.isMobile && state.app.sidebar.opened) {
      store.commit(TOGGLE_SIDEBAR, false)
    }
    next()
  }
})

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

const app = new Vue({
  router,
  store,
  nprogress,
  ...App
})

export { app, router, store }
